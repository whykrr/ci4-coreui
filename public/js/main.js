/* eslint-disable object-shorthand */

/* global Chart, coreui, coreui.Utils.getStyle, coreui.Utils.hexToRgba */

/**
 * --------------------------------------------------------------------------
 * CoreUI Boostrap Admin Template (v3.4.0): main.js
 * Licensed under MIT (https://coreui.io/license)
 * --------------------------------------------------------------------------
 */

/* eslint-disable no-magic-numbers */
// Disable the on-canvas tooltip
Chart.defaults.global.pointHitDetectionRadius = 1;
Chart.defaults.global.tooltips.enabled = false;
Chart.defaults.global.tooltips.mode = "index";
Chart.defaults.global.tooltips.position = "nearest";
Chart.defaults.global.tooltips.custom = coreui.ChartJS.customTooltips;
Chart.defaults.global.defaultFontColor = "#646470"; // eslint-disable-next-line no-unused-vars

var cardChart1 = new Chart(document.getElementById("card-chart1"), {
    type: "line",
    data: {
        labels: [
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
        ],
        datasets: [
            {
                label: "My First dataset",
                backgroundColor: "transparent",
                borderColor: "rgba(255,255,255,.55)",
                pointBackgroundColor: coreui.Utils.getStyle("--primary"),
                data: [1, 18, 9, 17, 34, 22, 11],
            },
        ],
    },
    options: {
        maintainAspectRatio: false,
        legend: {
            display: false,
        },
        scales: {
            xAxes: [
                {
                    gridLines: {
                        color: "transparent",
                        zeroLineColor: "transparent",
                    },
                    ticks: {
                        fontSize: 2,
                        fontColor: "transparent",
                    },
                },
            ],
            yAxes: [
                {
                    display: false,
                    ticks: {
                        display: false,
                        min: -4,
                        max: 39,
                    },
                },
            ],
        },
        elements: {
            line: {
                tension: 0.00001,
                borderWidth: 1,
            },
            point: {
                radius: 4,
                hitRadius: 10,
                hoverRadius: 4,
            },
        },
    },
}); // eslint-disable-next-line no-unused-vars

var cardChart2 = new Chart(document.getElementById("card-chart2"), {
    type: "line",
    data: {
        labels: [
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
        ],
        datasets: [
            {
                label: "My First dataset",
                backgroundColor: "transparent",
                borderColor: "rgba(255,255,255,.55)",
                pointBackgroundColor: coreui.Utils.getStyle("--info"),
                data: [1, 18, 9, 17, 34, 22, 11],
            },
        ],
    },
    options: {
        maintainAspectRatio: false,
        legend: {
            display: false,
        },
        scales: {
            xAxes: [
                {
                    gridLines: {
                        color: "transparent",
                        zeroLineColor: "transparent",
                    },
                    ticks: {
                        fontSize: 2,
                        fontColor: "transparent",
                    },
                },
            ],
            yAxes: [
                {
                    display: false,
                    ticks: {
                        display: false,
                        min: -4,
                        max: 39,
                    },
                },
            ],
        },
        elements: {
            line: {
                tension: 0.00001,
                borderWidth: 1,
            },
            point: {
                radius: 4,
                hitRadius: 10,
                hoverRadius: 4,
            },
        },
    },
}); // eslint-disable-next-line no-unused-vars

var cardChart3 = new Chart(document.getElementById("card-chart3"), {
    type: "line",
    data: {
        labels: [
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "Desember",
        ],
        datasets: [
            {
                label: "My First dataset",
                backgroundColor: "transparent",
                borderColor: "rgba(255,255,255,.55)",
                pointBackgroundColor: coreui.Utils.getStyle("--warning"),
                data: [0, 0, 0, 17, 34, 22, 11, 12, 29, 30, 11, 16],
            },
        ],
    },
    options: {
        maintainAspectRatio: false,
        legend: {
            display: false,
        },
        scales: {
            xAxes: [
                {
                    gridLines: {
                        color: "transparent",
                        zeroLineColor: "transparent",
                    },
                    ticks: {
                        fontSize: 2,
                        fontColor: "transparent",
                    },
                },
            ],
            yAxes: [
                {
                    display: false,
                    ticks: {
                        display: false,
                        min: -4,
                        max: 39,
                    },
                },
            ],
        },
        elements: {
            line: {
                tension: 0.00001,
                borderWidth: 1,
            },
            point: {
                radius: 4,
                hitRadius: 10,
                hoverRadius: 4,
            },
        },
    },
}); // eslint-disable-next-line no-unused-vars

var mainChart = new Chart(document.getElementById("main-chart"), {
    type: "line",
    data: {
        labels: [
            "Januari",
            "Februari",
            "Maret",
            "April",
            "Mei",
            "Juni",
            "Juli",
            "Agustus",
            "September",
            "Oktiber",
            "November",
            "Desember",
        ],
        datasets: [
            {
                label: "Tes",
                backgroundColor: "transparent",
                borderColor: coreui.Utils.getStyle("--info"),
                pointHoverBackgroundColor: "#fff",
                borderWidth: 2,
                data: [
                    2000000, 3400000, 7800000, 14080000, 13400000, 16750000,
                    19804000, 20500000,
                ],
            },
        ],
    },
    options: {
        maintainAspectRatio: false,
        legend: {
            display: false,
        },
        scales: {
            xAxes: [
                {
                    gridLines: {
                        drawOnChartArea: false,
                    },
                },
            ],
            yAxes: [
                {
                    ticks: {
                        beginAtZero: true,
                        maxTicksLimit: 5,
                    },
                },
            ],
        },
        elements: {
            line: {
                borderWidth: 2,
                tension: 0.00001,
            },
            point: {
                radius: 4,
                hitRadius: 10,
                hoverRadius: 4,
            },
        },
    },
});
//# sourceMappingURL=main.js.map
