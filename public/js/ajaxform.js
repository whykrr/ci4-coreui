var buttonSubmit = "Simpan";
const loading = `<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
Loading...`;

function loadDatatables(obj) {
    $(".loading-page").show();
    source = $(obj).data("source");
    var datatable = $(obj).DataTable();
    $.getJSON(source, function (returnData) {
        $(".loading-page").hide();
        datatable.clear();
        datatable.rows.add(returnData.data);
        datatable.draw();
    });
}

function loadingAjax(obj, action) {
    if (action === "show") {
        buttonSubmit = $(obj).find("button[type=submit]").html();
        $(obj).find(".modal-footer button").attr("disabled", true);
        $(obj).find("button[type=submit]").html(loading);
    } else {
        $(obj).find(".modal-footer button").attr("disabled", false);
        $(obj).find("button[type=submit]").html(buttonSubmit);
    }
}
function sucessHandle(form, data, respond_type) {
    loadingAjax(form, "hide");
    if (data.response == "validation_error") {
        form.find(":input").removeClass("is-invalid");
        $.each(data.validation.data, function (key, data) {
            form.find(":input[name=" + key + "]").addClass("is-invalid");
            form.find(":input[name=" + key + "]")
                .parent()
                .find(".invalid-feedback")
                .text(data);

            form.find("#place_" + key).prepend(
                '<div class="alert alert-danger clone-feedback">' +
                    data +
                    "</div>"
            );
        });
    } else {
        switch (respond_type) {
            case "reload":
                swal({
                    title: "Simpan Berhasil",
                    icon: "success",
                    button: "Tutup",
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                }).then(function () {
                    location.reload();
                });
                break;
            default:
                $("#modalSide").modal("hide");
                loadDatatables(".tableAjax");
                swal({
                    title: "Simpan Berhasil",
                    icon: "success",
                    button: "Tutup",
                });
        }
    }
}
function errorHandle(form) {
    loadingAjax(form, "hide");
    swal({
        title: "Simpan Gagal",
        text: "Periksa kembali koneksi anda !",
        icon: "error",
        button: "Tutup",
    });
}

$(".tableAjax").each(function () {
    var judul_error = "Gagal mengambil data";
    var desk_error = "Periksa kembali koneksi anda !";
    $(".loading-page").show();
    obj = $(this);
    source = $(obj).data("source");
    $.getJSON(source, function (returnData) {
        $(obj).DataTable({
            data: returnData.data,
            columns: returnData.columns,
            language: {
                lengthMenu: "Tampilkan _MENU_ data per halaman",
                zeroRecords: "Data tidak ditemukan",
                info: "Halaman _PAGE_ dari _PAGES_ halaman",
                infoEmpty: "Tidak ada data tersedia",
                infoFiltered: "(disaring dari _MAX_ total data)",
                search: "Cari :",
                paginate: {
                    first: "Pertama",
                    last: "Terakhir",
                    next: "Lanjut",
                    previous: "Kembali",
                },
            },
        });
        $(".loading-page").hide();
    }).fail(function (data) {
        $(".loading-page").hide();

        if (data.status == 500) {
            judul_error = 500;
            desk_error = "Server error !";
        }
        swal({
            title: judul_error,
            text: desk_error,
            icon: "error",
            button: "Tutup",
        });
    });
});

$("body")
    .on("submit", ".ajax", function () {
        form = $(this);
        uri_redirect = $(this).data("redirect");
        respond = $(this).data("respond");
        $.ajax({
            type: "post",
            url: $(this).attr("action"),
            data: $(this).serialize(),
            dataType: "json",
            beforeSend: loadingAjax(form, "show"),
            success: function (data) {
                sucessHandle(form, data, respond);
            },
            error: function () {
                errorHandle(form);
            },
        });
        return false;
    })
    .on("submit", ".ajax-multipart", function () {
        form = $(this);
        uri_redirect = $(this).data("redirect");
        respond = $(this).data("respond");
        $.ajax({
            type: "post",
            url: $(this).attr("action"),
            data: new FormData(this), //buat ngambil isi dari form
            contentType: false,
            cache: false,
            processData: false,
            dataType: "json",
            beforeSend: loadingAjax(form, "show"),
            success: function (data) {
                sucessHandle(form, data, respond);
            },
            error: function () {
                errorHandle(form);
            },
        });
        return false;
    });

$("body").on("click", ".ajax-del", function () {
    url = $(this).data("url");
    swal({
        text: "Apakah anda yakin akan menghapus data ini?",
        icon: "warning",
        buttons: ["Tidak", "Ya"],
        dangerMode: true,
    }).then((willDelete) => {
        if (willDelete) {
            $.ajax({
                type: "DELETE",
                url: url,
                success: function (data) {
                    swal("Data berhasil dihapus !", {
                        icon: "success",
                    });
                    loadDatatables(".tableAjax");
                },
                error: function () {
                    swal({
                        title: "Gagal menghapus data",
                        text: "Periksa kembali koneksi anda !",
                        icon: "error",
                    });
                },
            });
        }
    });
});
