<li class="c-sidebar-nav-item">
    <a class="c-sidebar-nav-link <?= ($menu == 'dashboard' ? 'c-active' : '') ?>" href="<?= base_url('dashboard'); ?>">
        <div class="c-sidebar-nav-icon">
            <i class="c-icon cil-speedometer"></i>
        </div>
        Dashboard
    </a>
</li>
<?php foreach (get_sidebar_menu() as $item_menu) : ?>
    <?php if ($item_menu['parent'] == 'root') : ?>
        <li class="c-sidebar-nav-item">
            <a class="c-sidebar-nav-link <?= ($menu == $item_menu['name'] ? 'c-active' : '') ?>" href="<?= base_url($item_menu['link']); ?>">
                <div class="c-sidebar-nav-icon">
                    <i class="<?= $item_menu['icon'] ?>"></i>
                </div>
                <?= $item_menu['display_name'] ?>
            </a>
        </li>
    <?php else : ?>
        <li class="c-sidebar-nav-item c-sidebar-nav-dropdown <?= ($menu == $item_menu['name'] ? 'c-show' : '') ?>">
            <a class="c-sidebar-nav-link c-sidebar-nav-dropdown-toggle <?= ($menu == $item_menu['name'] ? 'c-active' : '') ?>" href="#<?= $item_menu['name'] ?>">
                <div class="c-sidebar-nav-icon">
                    <i class="c-icon cil-user"></i>
                </div>
                <?= $item_menu['display_name'] ?>
            </a>
            <ul class="c-sidebar-nav-dropdown-items">
                <?php foreach (get_sidebar_submenu($item_menu['name']) as $item_submenu) : ?>
                    <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link <?= (@$submenu == $item_submenu['name'] ? 'c-active' : '') ?>" href="<?= base_url($item_submenu['link']); ?>"> <?= $item_submenu['display_name'] ?></a></li>
                <?php endforeach; ?>
            </ul>
        </li>
    <?php endif; ?>
<?php endforeach; ?>