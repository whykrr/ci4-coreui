<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class SidebarMenu extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type'           => 'INT',
                'constraint'     => 11,
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'name' => ['type' => 'varchar', 'constraint' => 100, 'null' => false],
            'parent' => ['type' => 'varchar', 'constraint' => 100, 'null' => false],
            'display_name' => ['type' => 'text', 'null' => false],
            'permission_name' => ['type' => 'varchar', 'constraint' => 100, 'null' => false],
            'link' => ['type' => 'text', 'null' => true],
            'icon' => ['type' => 'varchar', 'constraint' => 100, 'null' => true],
        ]);
        $this->forge->addKey('id', true);
        $this->forge->addKey('name');
        $this->forge->addKey('parrent');
        $this->forge->addKey('permission_name');
        $this->forge->createTable('sidebar_menus');
    }

    public function down()
    {
        $this->forge->dropTable('sidebar_menus');
    }
}
