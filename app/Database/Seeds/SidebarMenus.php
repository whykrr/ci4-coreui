<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class SidebarMenus extends Seeder
{
    public function run()
    {
        $model = model('SidebarMenuModel');
        $model->truncate();

        $data = [
            [
                'name' => 'master_user',
                'display_name' => 'Master User',
                'parent' => 'root_sub',
                'permission_name' => '',
                'link' => '',
                'icon' => 'cil cil-user',
            ],
            [
                'name' => 'data_user',
                'display_name' => 'Data User',
                'parent' => 'master_user',
                'permission_name' => '',
                'link' => 'user/master',
                'icon' => '',
            ],
            [
                'name' => 'permission',
                'display_name' => 'Data Hak Akses',
                'parent' => 'master_user',
                'permission_name' => '',
                'link' => 'user/permissions',
                'icon' => '',
            ],
            [
                'name' => 'master_product',
                'display_name' => 'Master Produk',
                'parent' => 'root_sub',
                'permission_name' => '',
                'link' => '',
                'icon' => 'cil cil-3d',
            ],
            [
                'name' => 'data_product',
                'display_name' => 'Data Produk',
                'parent' => 'master_product',
                'permission_name' => '',
                'link' => 'product/master',
                'icon' => '',
            ],
            [
                'name' => 'raw_material',
                'display_name' => 'Bahan Baku',
                'parent' => 'master_product',
                'permission_name' => '',
                'link' => 'product/materials',
                'icon' => '',
            ],
            [
                'name' => 'voucher',
                'display_name' => 'Voucher',
                'parent' => 'root',
                'permission_name' => '',
                'link' => 'vouchers',
                'icon' => 'cil cil-3d',
            ],
            [
                'name' => 'setting',
                'display_name' => 'Setting',
                'parent' => 'root',
                'permission_name' => '',
                'link' => 'setting',
                'icon' => 'cil cil-cog',
            ],
        ];

        $model->insertBatch($data);
    }
}
