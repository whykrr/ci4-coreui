<?php

namespace App\Controllers;

class Dashboard extends BaseController
{
	public function index()
	{
		$data['menu'] = 'dashboard';
		return view('dashboard/dashboard', $data);
	}

	//--------------------------------------------------------------------

}
