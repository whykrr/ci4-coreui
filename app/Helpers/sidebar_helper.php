<?php

/**
 * # --------------------------------------------------------------------
 * * Mengambil data sidebar menu
 * # --------------------------------------------------------------------
 *
 * @return array
 */
function get_sidebar_menu()
{
    $model = model('SidebarMenuModel');
    $get_menu = $model
        ->whereIn('parent', ['root', 'root_sub'])
        ->orderBy('id', 'asc')
        ->findAll();

    return $get_menu;
}

/**
 * # --------------------------------------------------------------------
 * * Mengambil data sidebar sub menu
 * # --------------------------------------------------------------------
 *
 * description
 *
 * @param string $parent
 *
 * @return array 
 */
function get_sidebar_submenu($parent = null)
{

    $model = model('SidebarMenuModel');
    $get_submenu = $model
        ->where('parent', $parent)
        ->orderBy('id', 'asc')
        ->findAll();

    return $get_submenu;
}
